<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 08.07.2015
 * Time: 16:15
 */

namespace SettingsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(
collection="devices"
 * )
 */
class Device {
	/**
	 * @MongoDB\Id
	 */
    private $id;

    /**
     * @MongoDB\String
     */
    private $user_id;

	/**
	 * @MongoDB\String
	 */
    private $name;

	/**
	 * @MongoDB\String
	 */
    private $os;


	/**
	 * @MongoDB\Collection
	 */
	private $sensors = array();


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get userId
     *
     * @return id $userId
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set os
     *
     * @param string $os
     * @return self
     */
    public function setOs($os)
    {
        $this->os = $os;
        return $this;
    }

    /**
     * Get os
     *
     * @return string $os
     */
    public function getOs()
    {
        return $this->os;
    }

    /**
     * Set sensors
     *
     * @param collection $sensors
     * @return self
     */
    public function setSensors($sensors)
    {
        $this->sensors = $sensors;
        return $this;
    }

    /**
     * Get sensors
     *
     * @return collection $sensors
     */
    public function getSensors()
    {
        return $this->sensors;
    }


    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }
}
