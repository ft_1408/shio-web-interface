<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 08.07.2015
 * Time: 17:30
 */

namespace SettingsBundle\Service;

use AppBundle\Service\User;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use SettingsBundle\SettingsBundle;

class Device
{
	/**
	 * @var DocumentRepository
	 */
	private $repository;
	/**
	 * @var Symfony password encoder
	 */

	private $dm;

	private $userService;

	public function __construct(DocumentRepository $repository, DocumentManager $dm, User $s)
	{
		$this->repository = $repository;
		$this->dm = $dm;
		$this->userService = $s;
	}

	public function addDevice(\SettingsBundle\Document\Device $device){
		$user = $this->userService->getCurrentUser();
		$currentUserId = $user->getId();

		$device->setUserId($currentUserId);

		$this->dm->persist($device);
		$this->dm->flush();

		return $device;
	}

	public function removeDevice($id){
		$device = $this->repository->find($id);

		$this->dm->remove($device);
		return $this->dm->flush();
	}

	/**
	 * Getting all devices
	 * @return array
	 */
	public function getAll(){
		// Doctrine ODM mapper don't working so using this
		$user = $this->userService->getCurrentUser();
		return $this->repository->createQueryBuilder()
			->field('user_id')->equals($user->getId())
			->getQuery();
	}


	/**
	 * Getting one device by id
	 * @param $id
	 * @return \SettingsBundle\Document\Device
     */
	public function get($id){
		return $this->repository->find($id);
	}

	public function edit($id, $deviceData){
		$device = $this->repository->find($id);
		$device->setName($deviceData['name']);
		$device->setOs($deviceData['os']);

		$this->dm->persist($device);
		$this->dm->flush();

		return $device;
	}
}