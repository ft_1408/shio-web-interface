<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 08.07.2015
 * Time: 16:31
 */
namespace SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SettingsBundle\Document\Device;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DeviceController extends Controller
{
	public function indexAction()
	{
		$deviceService = $this->get('device_service');

		$devices = $deviceService->getAll();

		return $this->render('SettingsBundle:Device:index.html.twig', ['devices' => $devices]);
	}

	public function addAction(Request $request){
		$deviceData = $request->request->get('device');
		$device = new Device();
		$response = array();
		$userService = $this->get('device_service');
		$serializer = $this->get('serializer');

		$device->setName($deviceData['name']);
		$device->setOs($deviceData['os']);

		$validator = $this->get('validator');
		$errors = $validator->validate($device);

		if(count($errors) > 0){
			$response['errors'] = $errors;
		}else{
			$newDevice = $userService->addDevice($device);

			$json = $serializer->serialize($newDevice, 'json');
			$response = $json;
		}

		return new JsonResponse($response);
	}

	public function deleteAction(Request $request){
		$id = $request->request->get('id');
		$device = new Device();
		$response = array();
		$userService = $this->get('device_service');

		$response = $userService->removeDevice($id);
		return new JsonResponse($response);
	}

	public function getAction($id, Request $request){
        $response = array();
        $deviceService = $this->get('device_service');
        $serializer = $this->get('serializer');

        if(!$id){
            $response['errors'] = 'No device id transfered';
        }else{
            $device = $deviceService->get($id);
            $json = $serializer->serialize($device, 'json');
            $response = $json;
        }
        
        return new JsonResponse($response);
    }

	public function editAction(Request $request){
		$deviceData = $request->request->get('device');
		$id = $request->request->get('id');
		$response = array();
		$deviceService = $this->get('device_service');
		$serializer = $this->get('serializer');

		$device = $deviceService->edit($id, $deviceData);

		$json = $serializer->serialize($device, 'json');
		$response = $json;

		return new JsonResponse($response);
	}
}