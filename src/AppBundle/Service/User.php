<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 24.06.2015
 * Time: 17:42
 */

namespace AppBundle\Service;

use AppBundle\AppBundle;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\DocumentManager;

class User{

	/**
	 * @var DocumentRepository
	 */
	private $repository;
	/**
	 * @var Symfony password encoder
	 */
	private $encoder;
	/**
	 * @var DocumentManager
	 */
	private $dm;

	private $tokenStorage;

	public function __construct(
						DocumentRepository $repository,
						$encoder,
						DocumentManager $dm,
						\Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage  $tokenStorage)
	{
		$this->repository = $repository;
		$this->encoder = $encoder;
		$this->dm = $dm;
		$this->tokenStorage = $tokenStorage;
	}

	/**
	 * @param \AppBundle\Document\User $user
	 * Add new user with password and secret key
	 * @return $this
	 */
	public function addUser(\AppBundle\Document\User $user)
	{
		$encoded = $this->encoder->encodePassword($user, $user->getPassword());
		$secretKey = md5($encoded . time() . $user->getName());

		$user->setPassword($encoded);
		$user->setSecretKey($secretKey);

		$this->dm->persist($user);
		$this->dm->flush();

		return $user;
	}

    /**
     * Removes user by ID
     * @param sting $id
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function removeUser($id){
        $user = $this->repository->find($id);

        $this->dm->remove($user);
        return $this->dm->flush();
    }


    /**
     * @param string $id
     * @return \AppBundle\Document\User
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function getUser($id){
        return $this->repository->find($id);
    }

    /**
     * @param $id
     * @param $userData
     * @return \AppBundle\Document\User
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function editUser($id, $userData){
        $user = $this->repository->find($id);

        if(!empty($userData['password'])){
            $encoded = $this->encoder->encodePassword($user, $userData['password']);
            $user->setPassword($encoded);
        }

        $user->setName($userData['name']);
        $user->setRoles(array($userData['role']));

        $this->dm->persist($user);
        $this->dm->flush();

        return $user;
    }

	/**
	 * Getting all users
	 * @return array
	 */
	public function getAll(){
		return $this->repository->findAll();
	}

	public function getCurrentUser(){
		return $this->tokenStorage->getToken()->getUser();
	}
}