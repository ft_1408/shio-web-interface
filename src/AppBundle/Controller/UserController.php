<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 25.06.2015
 * Time: 16:01
 */

namespace AppBundle\Controller;

use AppBundle\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(){
		$serviceUser = $this->get('user_service');
		$users = $serviceUser->getAll();

		return $this->render('user/index.html.twig', [
			'users'=>$users
		]);
	}

    /** Add user action
     * @param Request $request
     * @return JsonResponse
     */
    public function addAction(Request $request){

		$userData = $request->request->get('user');
		$user = new User();
        $response = array();
        $userService = $this->get('user_service');
        $serializer = $this->get('serializer');

		$user->setName($userData['name']);
		$user->setPassword($userData['password']);
		$user->setRoles(array($userData['role']));

        $validator = $this->get('validator');
        $errors = $validator->validate($user, null, array('registration'));

        if(count($errors) > 0){
            $response['errors'] = $errors;
        }else{
            $newUser = $userService->addUser($user);

            $json = $serializer->serialize($newUser, 'json');
            $response = $json;
        }

        return new JsonResponse($response);
	}

    /**
     * removeUser action
     * @param Request $request
     * @return JsonResponse
     */
    public function removeAction(Request $request){
        $responce = [];
        $userService = $this->get('user_service');
        $uid = $request->request->get('uid');
        $serializer = $this->get('serializer');

        $responce['data'] = $userService->removeUser($uid);

        return new JsonResponse($responce);
    }

    public function getAction(Request $request){
        $responce = [];
        $userService = $this->get('user_service');
        $serializer = $this->get('serializer');
        $uid = $request->request->get('uid');

        $user = $userService->getUser($uid);
        $json = $serializer->serialize($user, 'json');

        return new Response($json);
    }

    public function editAction(Request $request){
        $responce = [];
        $userService = $this->get('user_service');
        $serializer = $this->get('serializer');
        $userData = $request->request->get('user');
        $uid = $request->request->get('uid');

        $user = $userService->editUser($uid, $userData);
        $json = $serializer->serialize($user, 'json');

        return new Response($json);
    }
}