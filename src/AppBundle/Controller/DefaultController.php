<?php

namespace AppBundle\Controller;

use AppBundle\Document\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/app/example", name="homepage")
     */
    public function indexAction()
    {

//		$user = new User();
//		$user->setName('user');
//		$user->setPassword('user');


		$userService = $this->get('user_service');
//		$userService->addUser($user);

		return $this->render('default/index.html.twig');
    }

	public function homeAction(){
		return $this->render('default/home.html.twig');
	}
}
