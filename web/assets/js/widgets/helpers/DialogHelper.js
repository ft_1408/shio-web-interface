/**
 * Created by dmitriy on 13.07.2015.
 */

function DialogHelper($selector, $modalSelector, $parent){
    var $this = this;
    $this.selector = $selector;
    $this.widget = {};
    $this.parent = $parent;
    $this.modalSelector = $modalSelector;

    $this.init = function(){
        $this.widget = $($this.selector);

        // Ok button click for process
        $('body').on('click', '#ok-button', $this.onOkClick);
    }

    // Listener on OK click in dialog resolving parent process
    $this.onOkClick = function($e){

        // Parse data from dialog window
        var $data = $this.parseData();

        // If not empty parent process resolve it
        if(!$.isEmptyObject($this.parent.process)){
            $this.parent.process.resolve($data);
        }
    }

    // Creating dialog with transferred data
    $this.createDialog = function($data){
        // Getting HTML of current tpl
        $tpl = $this.widget.html();

        // Parse with Mustache
        Mustache.parse($tpl);

        // Attaching transferred data
        var $rendered = Mustache.render($tpl, $data);

        $($rendered).modal();

        // Defiding parent process property as deffered
        $this.parent.process = $.Deferred();

        // And returning it
        return $this.parent.process;
    }

    $this.close = function(){
        // Hiding dialog
        $($this.modalSelector).modal('hide');

        // Removing dialog window from DOM
        // we will left only template
        $($this.modalSelector).remove();
    }

    // "Stupid" data parser
    $this.parseData = function(){
        var $fields = $($this.modalSelector).find('input'),
            $data = {};

        $.each($fields, function($i, $field){
            var $el = $($field),
                $key = $el.attr('name'),
                $val = $el.val();

            $data[$key] = $val;
        });

        return $data;
    }

    $this.init();
}