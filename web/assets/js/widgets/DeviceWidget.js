/**
 * Created by dmitriy on 13.07.2015.
 */
/**
 * Created by dmitriy on 26.06.2015.
 */

function DeviceWidget($selector, $parent){
    var $this = this;
    $this.selector = $selector;
    $this.parent = $parent;

    $this.process = {};

    $this.init = function(){
        $this.widget = $($this.selector);

        // Dialog for editing and adding devices
        $this.devicesDialog = new DialogHelper('#deviceDialog', '#devicemodal', $this);
        $this.removeDialog = new DialogHelper('#deviceDelete', '#deletemodal', $this);

        $('body').on('click', 'button', $this.processButtonClick);
    }

    // Event listener 'data-action'
    $this.processButtonClick = function($e){
        var $button = $($e.target);
        if($action = $button.attr('data-action')){
            if(typeof $this[$action] == 'function'){
                $this[$action]($e);
            }else{
                console.log('Called function ' + $action + ' not exsists!');
            }

        }
    }

    $this.addDevice = function($e){
        console.log($e);
        var $result = $this.devicesDialog.createDialog(
                {'header':'Add new device'}
        );
        $result.done(function($data){
            $this.addNewDevice($data);
        });
    }

    $this.addNewDevice = function($data){
        if($data.name){
            $.ajax({
                dataType: 'json',
                method: 'PUT',
                data : {'device' : $data}

            }).success(function($data){
                var $data = JSON.parse($data);
                if(!$data.errors){
                    $this.addDeviceToTable($data);
                    $this.devicesDialog.close();
                }else{
                    // TODO: implement error messages
                    return false
                }
            });
        }else{
            // TODO: implement error messages
            return false
        }
    }

    $this.addDeviceToTable = function($device){
        var $devicesTable = $this.widget.find('#drvices-table tbody');
        $tpl = $('#deviceRow').html();

        Mustache.parse($tpl);
        var $rendered = Mustache.render($tpl, $device);

        $devicesTable.append($rendered);
    }

    $this.removeDevice = function($e){
        $el  = $($e.target),
            $id = $el.attr('data-id'),
            $row = $($($el.parent()).parent()),
            $user = {};

        $row = $($row.parent());

        $user.name = $row.find('.deviceName').html();

        $result = $this.removeDialog.createDialog($user);
        $result.done(function(){
            $this.rmDevice($id, $row);
        });
    }

    $this.rmDevice = function($id, $dataRow){
        $.ajax({
            dataType: 'json',
            method: 'DELETE',
            data : {'id' : $id}

        }).success(function($data){
            if(!$data.errors){
                $dataRow.remove();
                $this.removeDialog.close();
            }else{
                // TODO: implement error messages
                return false
            }
        });
    }


    //$this.editUserDialog = function($user){
    //    $tpl = $this.userDialog.html();
    //    Mustache.parse($tpl);
    //
    //    $user.header = 'Edit user';
    //    $user.roles = $this.userRoles
    //
    //    var $rendered = Mustache.render($tpl, $user);
    //
    //    $($rendered).modal();
    //
    //    $this.process = $.Deferred();
    //
    //    return $this.process;
    //}
    //
    $this.editDevice = function($id, $data){

        if($data.name){
            return $.ajax({
                dataType: 'json',
                method: 'POST',
                data : {
                    "id"  : $id,
                    "device" : $data
                }
            });
        }else{
            // TODO: implement error messages
            return false
        }

    }
    //
    //$this.removeDialog = function($user){
    //    var $tpl = $tpl = $('#userDelete').html();
    //
    //    Mustache.parse($tpl);
    //    var $rendered = Mustache.render($tpl, $user);
    //
    //    $($rendered).modal();
    //
    //    $this.process = $.Deferred();
    //
    //    return $this.process;
    //}
    //
    $this.getUserById = function($uid) {
        var $process = $.ajax({
            dataType: 'json',
            method: 'GET',
            url : window.location.href  + $uid,

        });

        return $process;
    }

    $this.renewRow = function($data, $row){
        console.log($data);
        console.log($row);
        $row.find('.deviceName').html($data.name);
        $row.find('.deviceOs').html($data.os);
    }

    $this.edit = function($e){
        $el  = $($e.target),
            $id = $el.attr('data-id'),
            $row = $($($($el.parent()).parent()).parent());

        var $process = $this.getUserById($id);
        $process.success(function($data){
            if(!$data.errors){
                var $data = JSON.parse($data);
                var $result = $this.devicesDialog.createDialog($data);

                $result.done(function($data){
                    var $result = $this.editDevice($id, $data);

                    if($result){
                        $result.success(function($data){
                            var $data = JSON.parse($data);
                            if(!$data.errors){

                                $this.renewRow($data, $row);

                                $this.devicesDialog.close();
                            }
                        });
                    }
                });
            }
        });
    }

    $this.init();
}