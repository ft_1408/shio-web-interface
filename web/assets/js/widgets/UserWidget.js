/**
 * Created by dmitriy on 26.06.2015.
 */

function UserWidget($selector, $parent){
    var $this = this;
    $this.selector = $selector;
    $this.parent = $parent;
    $this.userRoles = [
        {value: "ROLE_USER", name: 'user'},
        {value: "ROLE_ADMIN", name: 'admin'}
    ];
    $this.process = {};

    $this.init = function(){
        $this.widget = $($this.selector);

        $this.buttons = $this.widget.find('button');
        $this.userDialog = $('#userDialog');

        $('body').on('click', 'button', $this.processButtonClick);

        // Ok button click for process
        $('body').on('click', '#ok-button', $this.onOkClick);
    }

    $this.onOkClick = function($e){
        if(!$.isEmptyObject($this.process)){
            $this.process.resolve();
        }
    }

    // Event listener 'data-action'
    $this.processButtonClick = function($e){
        var $button = $($e.target);

        if($action = $button.attr('data-action')){

            if(typeof $this[$action] == 'function'){
                $this[$action]($e);
            }else{
                console.log('Called function ' + $action + ' not exsists!');
            }

        }
    }

    $this.addUser = function($e){
        $result = $this.newUserDialog();

        $result.done(function(){
            $this.addNewUser();
        });
    }

    $this.addNewUser = function(){
        var $modal = $('#usermodal'),
            $name = $modal.find('[name="name"]'),
            $password = $modal.find('[name="password"]'),
            $role = $modal.find('[name="role"]');

        if($name && $password && $role){
            $user = {
                'name' : $name.val(),
                'password' : $password.val(),
                'role' : $role.val()
            }

            $.ajax({
                url: "/app_dev.php/admin/users/add/",
                dataType: 'json',
                method: 'POST',
                data : {'user' : $user}

            }).success(function($data){
                var $data = JSON.parse($data);
                if(!$data.errors){
                    $this.addUserToTable($data);

                    $('#usermodal').modal('hide');
                    $('#usermodal').remove();
                }else{
                    // TODO: implement error messages
                    return false
                }
            });
        }else{
            // TODO: implement error messages
            return false
        }
    }

    $this.addUserToTable = function($user){
        var $usersTable = $this.widget.find('#users-table tbody');
        $tpl = $('#userRow').html();

        Mustache.parse($tpl);
        var $rendered = Mustache.render($tpl, $user);

        $usersTable.append($rendered);
    }

    $this.remove = function($e){
        $el  = $($e.target),
        $uid = $el.attr('data-id'),
        $row = $($($el.parent()).parent()),
        $user = {};

        $user.name = $row.find('.userName').html();

        $result = $this.removeDialog($user);
        $result.done(function(){
            $this.removeUser($uid, $row);
        });
    }

    /**
     * removeUser from db
     * @param $uid string user id
     * @param $dataRow jquerry object row to delete
     */
    $this.removeUser = function($uid, $dataRow){
        $.ajax({
            url: "/app_dev.php/admin/users/remove/",
            dataType: 'json',
            method: 'POST',
            data : {'uid' : $uid}

        }).success(function($data){
            if(!$data.errors){
                console.log('removed user '+ $uid);
                $dataRow.remove();
                $('#deletemodal').modal('hide');
                $('#deletemodal').remove();
            }else{
                // TODO: implement error messages
                return false
            }
        });
    }

    $this.newUserDialog = function(){
        $tpl = $this.userDialog.html();
        Mustache.parse($tpl);

        var $rendered = Mustache.render($tpl, {
            header: 'Add user',
            roles: $this.userRoles
        });

        $($rendered).modal();

        $this.process = $.Deferred();

        return $this.process;
    }

    $this.editUserDialog = function($user){
        $tpl = $this.userDialog.html();
        Mustache.parse($tpl);

        $user.header = 'Edit user';
        $user.roles = $this.userRoles

        var $rendered = Mustache.render($tpl, $user);

        $($rendered).modal();

        $this.process = $.Deferred();

        return $this.process;
    }

    $this.editUser = function($data){

        if($data.name){
            return $.ajax({
                url: "",
                dataType: 'json',
                method: 'POST',
                data : {
                    "id": $uid,
                    "device" : $data
                }

            });
        }else{
            // TODO: implement error messages
            return false
        }
    }

    $this.removeDialog = function($user){
        var $tpl = $tpl = $('#userDelete').html();

        Mustache.parse($tpl);
        var $rendered = Mustache.render($tpl, $user);

        $($rendered).modal();

        $this.process = $.Deferred();

        return $this.process;
    }

    $this.getUserById = function($uid) {
        var $process = $.ajax({
            url: "/app_dev.php/admin/users/get/",
            dataType: 'json',
            method: 'POST',
            data : {'uid' : $uid}

        });

        return $process;
    }

    $this.renewRow = function($data, $row){
        $row.find('.userName').html($data.name);
        $row.find('.userRoles').html($data.roles[0]);
    }

    $this.edit = function($e){
        $el  = $($e.target),
        $uid = $el.attr('data-id'),
        $row = $($($el.parent()).parent());

        var $process = $this.getUserById($uid);
        $process.success(function($data){
            if(!$data.errors){
                var $result = $this.editUserDialog($data);
                $result.done(function(){
                    var $result = $this.editUser($uid);

                    if($result){
                        $result.success(function($data){
                            if(!$data.errors){

                                $this.renewRow($data, $row);

                                $('#usermodal').modal('hide');
                                $('#usermodal').remove();
                            }
                        });
                    }
                });
            }
        });
    }

    $this.init();
}